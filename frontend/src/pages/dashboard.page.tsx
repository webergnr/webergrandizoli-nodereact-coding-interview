import React, { FC, useState, useEffect, useMemo } from "react";

import { RouteComponentProps } from "@reach/router";
import { IUserProps } from "../dtos/user.dto";
import { UserCard } from "../components/users/user-card";
import { CircularProgress } from "@mui/material";

import { BackendClient } from "../clients/backend.client";

const ITEMS_PER_PAGE = 10;

const backendClient = new BackendClient();

export const DashboardPage: FC<RouteComponentProps> = () => {
  const [users, setUsers] = useState<IUserProps[]>([]);
  const [loading, setloading] = useState<boolean>(true);
  const [currentPage, setCurrentPage] = useState<number>(95);

  useEffect(() => {
    const fetchData = async () => {
      const result = await backendClient.getAllUsers();
      setUsers(result.data);
      setloading(false);
    };

    fetchData();
  }, []);

  const currentItems = useMemo(() => {
    return users.slice(
      (currentPage - 1) * ITEMS_PER_PAGE,
      currentPage * ITEMS_PER_PAGE
    );
  }, [currentPage, users]);

  const handlePreviousPage = () => {
    if (currentPage === 1) return;
    setCurrentPage((s) => s - 1);
  };

  const handleNextPage = () => {
    const totalPages = Math.ceil(users.length / ITEMS_PER_PAGE);
    if (currentPage === totalPages) return;
    setCurrentPage((s) => s + 1);
  };

  return (
    <div style={{ paddingTop: "30px" }}>
      <div style={{ display: "flex", justifyContent: "space-between" }}>
        {loading ? (
          <div
            style={{
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
              height: "100vh",
              width: "100vw",
            }}
          >
            <CircularProgress size="60px" />
          </div>
        ) : (
          <div>
            {currentItems.length
              ? currentItems.map((user) => {
                  return <UserCard key={user.id} {...user} />;
                })
              : null}
            <div>
              <button onClick={handlePreviousPage}> previous </button>
              <button onClick={handleNextPage}>next </button>
            </div>
          </div>
        )}
      </div>
    </div>
  );
};
